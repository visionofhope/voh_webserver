<?php
header('Content-Type: text/html; charset=utf-8');
/*======================================================================*\
|| #################################################################### ||
|| # STAR WARS GALAXIES						      					  # ||
|| # SWG: RESURGENCE AUTHENTICATION SYSTEM					    	  # ||
|| # https://swgresurgence.com									      # ||
|| # 								     							  # ||
|| # Developer:		Dark					 					      # ||
|| #			    dark@swgresurgency.com	                          # ||
|| #                       											  # ||
|| # Version: 		5.0                      	                      # ||
|| # Release Date:	August 08, 2018 							      # ||
|| #								    							  # ||
|| # Copyright © Star Wars Galaxies: Resurgence		      	          # ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # 								     							  # ||
|| # Date:			06/26/2019			      						  # ||
|| # Comments:		live_server_auth.php				           	  # ||
|| #################################################################### ||
\*======================================================================*/

// #######################################################################
// ####################### SET PHP ENVIRONMENT ###########################
// #######################################################################

ini_set('display_errors', 1);

date_default_timezone_set('America/Chicago');

chdir('/home3/swgresurg/public_html/forums'); 
require('global.php');
require('includes/functions_login.php');

//$BLOCK_VMS = false;

// #######################################################################
// ######################### FUNCTIONS ###################################
// #######################################################################

function hexToStr ($hex) {
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2) {
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}

function is_banned_guid($guid) {
    $log = file_get_contents('/logs/live_server_bad_guids_auth_log.txt');
    return strpos($log, $guid) !== false;
}

function get_architecture($str) {
    if (strpos($str, 'n') !== false) {
        return 'NATIVE';
    } elseif (strpos($str, 'v') !== false) {
        return 'VIRTUAL';
    } else {
        return 'N/A';
    }
}

// #######################################################################
// ####################### POST GET ITEMS ################################
// #######################################################################

$username = urldecode($_GET['un']);
$password = urldecode($_GET['pw']);
$ip = urldecode($_GET['ip']);
$suid = urldecode($_GET['suid']);

// #######################################################################
// ##################### NO INFO SPECIFIED ###############################
// #######################################################################

if(is_null($username) && is_null($password)) {
    die('No Login Provided');
}

$decoded = base64_decode($password);
$decoded = str_rot13($decoded);
$decoded = hexToStr($decoded);

$guid = '';
$architecture = '';
$real_password = '';

if (strlen($decoded) < 12 || strpos($decoded, "\n" === false)) {
    $result = 0;
} else {
    $parts = explode("\n", $decoded);
    $guid = $parts[0];
    $architecture = get_architecture($parts[1]);
    $real_password = $parts[2];
    
    if (is_banned_guid($guid) === true) {
        $auth_content = '[' . date('m/d/Y h:i:s a') . '] ' . 'Username: ' . $username . ', Station ID: ' . $suid . ', IP: ' . $ip . ', Architecture: ' . $architecture . ', GUID: ' . $guid . "\n";
		file_put_contents('/logs/live_server_bad_guid_auth_log.txt', $auth_content, FILE_APPEND);
        $result = 2;
    } elseif ($architecture === 'VIRTUAL' && $BLOCK_VMS === true) {
        ', Architecture: ' . $architecture . ', GUID: ' . $guid . "\n";$auth_content = '[' . date('m/d/Y h:i:s a') . '] ' . 'Username: ' . $username . ', Station ID: ' . $suid . ', IP: ' . $ip . 
		file_put_contents('/logs/live_server_bad_guid_auth_log.txt', $auth_content, FILE_APPEND);
        $result = 0;
    } else {
        
    }
}

$result = verify_authentication($username, $password, md5($password), md5($password), '', false);

// #######################################################################
// ####################### FINAL GET ID ##################################
// #######################################################################

if($result === true && !(strpos($username, '@') !== false)){
	if($vbulletin->userinfo['usergroupid'] == 8){
		$result = 2; // Banned Player.
	}elseif($vbulletin->userinfo['usergroupid'] == 4){
		$result = 3; // Moderated Player.
	}elseif($vbulletin->userinfo['usergroupid'] == 16){
		$result = 4; // COPPA Player.
	}elseif($vbulletin->userinfo['usergroupid'] == 3){
		$result = 5; // Awaiting Email Activation.
	}elseif($vbulletin->userinfo['usergroupid'] == 11){
		$result = 6; // Staff Player.
	//}elseif($vbulletin->userinfo['usergroupid'] == 2) {
	//	$result = 1; // Player.
	}else{
		$result = 1; // Unregistered Player.
	}
}else{
	$result = 0; // Incorrect Username and Password Combination.
}

// #######################################################################
// ####################### AUTHENTICATION LOGS ###########################
// #######################################################################

$auth_content = '[' . date('m/d/Y h:i:s a') . '] ' . 'Username: ' . $username . ', Station ID: ' . $suid . ', Result: ' . $result . ', IP: ' . $ip . ', Architecture: ' . $architecture . ', GUID: ' . $guid . "\n";
chdir('/home/swgresurg/public_html'); 
file_put_contents('logs/live_server_auth_log.txt', $auth_content, FILE_APPEND);
echo $result;
die();

// #######################################################################
// ####################### END OF FILE ###################################
// #######################################################################
?>